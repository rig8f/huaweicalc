// a stripped CLI-only version of main.cpp
#include "encrypt.h"
#include <unistd.h>

int main(int argc, char* argv[]) {
  char outbuf[40]; // buffers
  char imeibuf[16];
  int choice = 0;  // flag
  int reverse = 0; // flag

  int opt;
  while ((opt = getopt(argc, argv, "rRfF123hH")) != -1) {
    switch (opt) {
      case 'h':
      case 'H':
        // print help and exit
        printf("\n\n *** HUAWEI modems unlock codes calculator ***\n\n\
        %s [-f123] IMEI\n\n\
        -r - reverse IMEI\n\
        -f - only flash code\n\
        -1 - only code v1\n\
        -2 - only code v2\n\
        -3 - only code v201 (v3)\n\n", argv[0]);
        return 0;
      case 'f':
      case 'F': choice = 1; break;
      case '1': choice = 2; break;
      case '2': choice = 3; break;
      case '3': choice = 4; break;
      case 'r':
      case 'R': reverse = 1; break;
    }
  }   

  if (optind >= argc)
  { printf("\n IMEI not specified\n\n"); return 0; }

  strcpy(imeibuf, argv[optind]);

  // reverse IMEI if requested
  if (reverse != 0) {
    char c;
    for (int i = 0; i < 7; i++) {
      c = imeibuf[i];
      imeibuf[i] = imeibuf[14-i];
      imeibuf[14-i] = c;
    }
  }

  if (strlen(imeibuf) != 15)
  { printf("\n Wrong IMEI length\n"); return 0; }

  switch (choice) {
    case 0: {
      // only IMEI provided, print all numbers
      encrypt_v1(imeibuf, outbuf, (char*)"e630upgrade");
      printf("\n Flash code        = %s", outbuf);

      encrypt_v1(imeibuf, outbuf, (char*)"hwe620datacard");
      printf("\n Encrypt_v1 code   = %s", outbuf);

      calc2(imeibuf, outbuf);
      printf("\n Encrypt_v2 code   = %s", outbuf);

      calc201(imeibuf, outbuf);
      printf("\n Encrypt_v201 code = %s\n\n", outbuf);
      return 0;
    }

    // display only requested number
    case 1: { encrypt_v1(imeibuf, outbuf, (char*)"e630upgrade"); break; }
    case 2: { encrypt_v1(imeibuf, outbuf, (char*)"hwe620datacard"); break; }
    case 3: { calc2(imeibuf, outbuf); break; }
    case 4: { calc201(imeibuf, outbuf); break; }
  }
  printf("%s\n", outbuf);
}
