# built to compile and execute the CLI-only version
.PHONY: compile

compile:
	gcc -o huawei main-cli.cpp calc.cpp encrypt*.cpp -lcrypto
