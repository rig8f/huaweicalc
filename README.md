# HuaweiCalc

Huawei modems' flash and unlock codes calculator.

This repository is a fork of the original on GitHub: [forth32/huaweicalc](https://github.com/forth32/huaweicalc).

File `main-cli.cpp` is an adaptation of `main.cpp` that keeps only the CLI mode leaving out the GUI part:
this file and the `Makefile` are the only things that differ from the original code, that remains untouched.

## Dependencies

Both versions depends on OpenSSL for the MD5 algorithm (on a Debian-based system, be sure to install package `libssl-dev`).
Full version with GUI also depends on Qt5 libraries for QtApplication.

## Compilation

For the CLI-only version, via `make compile` or directly with

```bash
gcc -o huawei main-cli.cpp calc.cpp encrypt*.cpp -lcrypto
```

## Usage

`./huawei [-f123] IMEI` where:
```
    -h - print help and exit
    -r - reverse IMEI
    -f - print only flash code
    -1 - print only v1 code
    -2 - print only v2 code
    -3 - print only v201 (v3) code
    no options - print all codes
```
